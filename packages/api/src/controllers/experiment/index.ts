import litSigningCondition from "./lit-signing-condition";

export default {
  "lit-signing-condition": litSigningCondition,
};
